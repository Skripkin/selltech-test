import { ApolloClient, InMemoryCache } from '@apollo/client'
import { BatchHttpLink } from '@apollo/client/link/batch-http'

const API_URL = '/graphql-playground'

const link = new BatchHttpLink({ uri: API_URL })

const client = new ApolloClient({
  link: link,
  cache: new InMemoryCache(),
})

export default client
