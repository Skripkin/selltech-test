import styled from "@emotion/styled"

export default styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  width: 100%;
  background-color: #f5f6f7;
`
