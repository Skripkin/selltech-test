import React, { useState } from 'react'

import Autocomplete from '@mui/material/Autocomplete'
import TextField from '@mui/material/TextField'
import Box from '@mui/material/Box'

import PaperComponentWrapper from './PaperComponent'

type Option = {
  id: string
  name: string
}

interface CustomSelectProps {
  options: Option[]
  label: string
  onAddNewItem: (newItem: { id: string, name: string }) => void
  error?: string
  className?: string
}

const CustomSelect: React.FC<CustomSelectProps> = ({ options, label, onAddNewItem, error, className }) => {
  const [value, setValue] = useState<Option | null>(null)
  const [inputValue, setInputValue] = useState('')
  const [addingNewItem, setAddingNewItem] = useState(false)

  const handleAddNewItem = () => {
    onAddNewItem({ id: `${Math.random()}`, name: inputValue })
    setValue({ id: '', name: inputValue })
    setInputValue('')
    setAddingNewItem(false)
  }

  const handleCancelAddNewItem = () => {
    setAddingNewItem(false)
    setInputValue('')
  }

  const renderInput = (params: any) => {
    const { InputProps, ...rest } = params
    return (
      <TextField
        {...rest}
        InputProps={{
          ...InputProps,
          className: error ? 'Mui-error' : InputProps?.className
        }}
        label={label}
        variant="outlined"
        error={Boolean(error)}
        helperText={error}
      />
    )
  }

  return (
    <Box className={className}>
      <Autocomplete
        value={value}
        inputValue={inputValue}
        onChange={(event, newValue) => {
          setValue(newValue)
        }}
        onInputChange={(event, newInputValue) => {
          setInputValue(newInputValue)
        }}
        options={options}
        filterOptions={(options) =>
          options.filter((option) => option.name.toLowerCase().indexOf(inputValue.toLowerCase()) !== -1)
        }
        renderInput={renderInput}
        noOptionsText=""
        PaperComponent={PaperComponentWrapper}
      />
    </Box>
  )
}

export default CustomSelect
