import React, { useState, ReactNode } from "react"
import { Paper, Button, Box } from "@material-ui/core"

interface PaperComponentWrapperProps {
  children?: ReactNode
  [key: string]: any
}

const PaperComponentWrapper = ({
  children,
  ...props
}: PaperComponentWrapperProps) => {
  const [addingNewItem, setAddingNewItem] = useState(false)

  const handleAddNewItem = () => {
    setAddingNewItem(false)
  }

  const handleCancelAddNewItem = () => {
    setAddingNewItem(false)
  }

  return (
    <Paper {...props}>
      {addingNewItem ? (
        <Box mt={1} display="flex">
          <Button size="small" onClick={handleAddNewItem}>
            Confirm
          </Button>
          <Button size="small" onClick={handleCancelAddNewItem}>
            Cancel
          </Button>
        </Box>
      ) : (
        <>
          {children}
          <Box display="flex" alignItems="center" justifyContent="center" py={2}>
            <Button onClick={() => setAddingNewItem(true)}>Add new</Button>
          </Box>
        </>
      )}
    </Paper>
  )
}

export default PaperComponentWrapper
