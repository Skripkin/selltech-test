import { useQuery } from '@apollo/client'

import Relations from '@api/queries/Relations'
import { RelationsQuery } from '@gql/graphql'

const useRelations = () => {
  const { data = {}, ...rest } = useQuery<RelationsQuery>(Relations)

  const relations = data.applicantIndividualCompanyRelations?.data || []

  return { relations, ...rest }
}

export default useRelations
