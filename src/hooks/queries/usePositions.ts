import { useQuery } from '@apollo/client'

import Positions from '@api/queries/Positions'
import { PositionsQuery } from '@gql/graphql'

const usePositions = () => {
  const { data = {}, ...rest } = useQuery<PositionsQuery>(Positions)

  const positions = data.applicantIndividualCompanyPositions?.data || []

  return { positions, ...rest }
}

export default usePositions
