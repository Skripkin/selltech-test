import gql from 'graphql-tag'

export default gql`
  query Positions {
    applicantIndividualCompanyPositions {
      data {
        id
        name
      }
    }
  }
`