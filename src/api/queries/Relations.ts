import gql from 'graphql-tag'

export default gql`
  query Relations {
    applicantIndividualCompanyRelations {
      data {
        id
        name
      }
    }
  }
`
