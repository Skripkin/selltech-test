import React from 'react'
import { Button, TextField, CircularProgress, Grid } from '@mui/material'

import { Container, CustomSelect } from '@components'

import { useHome, useHomeForm } from './hooks'
import { Content, LoaderWrapper, TextArea, SubmitButton } from './Home.styles'

const Home: React.FC = () => {
  const { relations, positions, loading } = useHome()
  const form = useHomeForm()

  if (loading) {
    return (
      <Container>
        <Content>
          <LoaderWrapper>
            <CircularProgress size={20} />
          </LoaderWrapper>
        </Content>
      </Container>
    )
  }

  return (
    <Container>
      <Content component="form" onSubmit={form.handleSubmit}>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <CustomSelect
              options={relations}
              label="Relation"
              onAddNewItem={() => null}
              error={form.relationError}
            />
          </Grid>
          <Grid item xs={6}>
            <CustomSelect
              options={positions}
              label="Position"
              onAddNewItem={() => null}
              error={form.positionError}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              label="Email"
              {...form.email}
              error={!!form.emailError}
              helperText={form.emailError}
            />
          </Grid>
          <Grid item xs={12}>
            <TextArea
              aria-label="minimum height"
              minRows={6}
              placeholder="Description"
              {...form.description}
            />
          </Grid>
        </Grid>

        <SubmitButton type="submit">
          Submit
        </SubmitButton>
      </Content>
    </Container>
  )
}

export default Home
