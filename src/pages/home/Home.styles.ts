import styled from "@emotion/styled"
import { Box, TextareaAutosize, Button } from '@mui/material'

export const Content = styled(Box)`
  display: flex;
  flex-direction: column;
  margin: auto;
  padding: 2rem;
  max-width: 680px;
  width: 100%;
  border-radius: 8px;
  border: 1px solid #F1F1F6;
  background-color: white;
`

export const LoaderWrapper = styled.div`
  display: flex;
  justify-content: center;
`

export const TextArea = styled(TextareaAutosize)`
  padding: 16.5px 14px;
  width: 100%;
  box-sizing: border-box;
  border-radius: 4px;
  border-color: #c4c4c4;
  font: inherit;
`

export const SubmitButton = styled(Button)`
  margin-top: 2rem;
  margin-right: auto;
  margin-left: auto;
  width: 100%;
  max-width: 180px;
  box-shadow: none;
  background: #f1f1f6;
  color: black;

  &:hover {
    background: #f1f1f6;
    box-shadow: 0 0 10px 2px #f1f2f3;
  }

  &:disabled: {
    background: #f1f1f6;
    pointer-events: none;
    box-shadow: none;
  }
`
