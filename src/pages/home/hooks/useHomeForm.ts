import { useCallback } from 'react'
import { useForm } from 'react-hook-form'

interface FormValues {
  relation: string
  position: string
  email: string
  name: string
  description?: string
}

const useHomeForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormValues>()
  
  const handleFormSubmit = useCallback((data: FormValues) => {
    console.log('Data: ', data)
  }, [])

  return {
    handleSubmit: handleSubmit(handleFormSubmit),

    relation: register('relation', { required: true }),
    relationError: errors.relation ? 'Relation is required' : '',

    position: register('position', { required: true }),
    positionError: errors.relation ? 'Position is required' : '',

    email: register('email', { required: true, pattern: /\S+@\S+\.\S+/ }),
    emailError: errors.email
      ? errors.email.type === 'required'
        ? 'Email is required'
        : 'Invalid email address'
      : '',

    name: register('name', { required: true }),
    nameError: errors.name ? 'Name is required': '',

    description: register('description')
  }
}

export default useHomeForm
