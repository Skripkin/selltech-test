import { usePositions, useRelations } from '@hooks/queries'

const useHome = () => {
  const { positions, loading: positionsLoading } = usePositions()
  const { relations, loading: relationsLoading } = useRelations()

  return {
    positions,
    relations,
    loading: positionsLoading || relationsLoading
  }
}

export default useHome
