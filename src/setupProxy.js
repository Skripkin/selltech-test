const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = function(app) {
  app.use(
    '/graphql-playground',
    createProxyMiddleware({
      target: 'http://152.228.215.94:83/graphql-playground',
      changeOrigin: true,
      secure: false
    })
  )
}
