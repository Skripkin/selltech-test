import React from 'react'
import { createRoot } from 'react-dom/client'
import { ApolloProvider } from '@apollo/client'

import client from './apolloClient'
import GlobalStyles from './globalStyles'

import Home from './pages/home'

const container = document.getElementById('root')!
const root = createRoot(container)

root.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <GlobalStyles />
      <Home />
    </ApolloProvider>
  </React.StrictMode>
)
